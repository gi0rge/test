package com.example.quiz

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.RadioButton
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var radioRight: RadioButton
    private lateinit var checkBoxRight_1: CheckBox
    private lateinit var checkBoxRight_2: CheckBox
    private lateinit var checkBoxWrong_1: CheckBox
    private lateinit var userAnswer_3: EditText
    private val totalPoint = 3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        radioRight = findViewById(R.id.radioRight)
        checkBoxRight_1 = findViewById(R.id.checkBoxRight_1)
        checkBoxRight_2 = findViewById(R.id.checkBoxRight_2)
        checkBoxWrong_1 = findViewById(R.id.checkBoxWrong_1)
        userAnswer_3 = findViewById(R.id.userAnswer_3)
    }

    fun calcuatePoint(View: View) {
        var point = 0
        if(radioRight.isChecked) {
            point += 1
        }
        if(checkBoxRight_1.isChecked && checkBoxRight_2.isChecked && !checkBoxWrong_1.isChecked) {
            point += 1
        }
        if(userAnswer_3.text.toString() == "თბილისი") {
            point += 1
        }
        val result = "თქვენი ქულაა: ${point.toString()}/${totalPoint.toString()}"
        Toast.makeText(applicationContext, result, Toast.LENGTH_LONG).show()
    }
}